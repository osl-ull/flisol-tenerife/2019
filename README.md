# 2019

Aportaciones para la celebración del Flisol 2019, en Tenerife.

- Presentaciones
  - Grupo de Usuarios de Lunux de Canarias (Gulic)
  - Software Libre y Distribuciones
  - FreeCAD
  - Audacity + Shotcut
  - Terminal
  - Género, Software Libre y Conocimiento Abierto
 - Fotos del evento.
