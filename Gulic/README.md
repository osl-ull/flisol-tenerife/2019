# La presentación

Este minidocumento, describe como visualizar la presentación que realizamos para Gulic.

He utilizado el proyecto [Reveal](https://github.com/hakimel/reveal.js/) y [esta documentación](https://bookdown.org/yihui/rmarkdown/display-modes-2.html), para realizar esta presentación.

Para visualizar la presentación, primero lanzamos un servidor web, en local, usando python. 

El comando para lanzar el servidor web python, desde una terminal, sería este:  

```python
python3 -m http.server 8000
```

Luego abrimos un navegador, con soporte javascript, y, por último, le pasamos la siguiente dirección: [http://127.0.0.1:8000](http://127.0.0.1:8000/)

Automáticamente deberías ver la primera de las diapositivas :-)

Usando la misma presentación, he generado un documento en PDF, como alternativa en caso de que no logres visualizar la presentación correctamente.

Espero que les guste.  


Luis Cabrera Sauco
